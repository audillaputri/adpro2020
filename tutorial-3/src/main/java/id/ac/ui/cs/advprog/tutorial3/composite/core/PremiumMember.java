package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    private String name;
    private String role;
    private List<Member> memberList;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        this.memberList = new ArrayList<>();
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void addChildMember(Member member) {
        if(this.memberList.size()<3 || role.equals("Master")){ this.memberList.add(member);}
        else{}
    }

    @Override
    public void removeChildMember(Member member) {
        this.memberList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.memberList;
    }

    //TODO: Complete me
}
