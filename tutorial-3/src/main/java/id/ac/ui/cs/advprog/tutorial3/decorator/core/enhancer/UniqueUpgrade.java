package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;
    Random rdm = new Random();

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        int x = rdm.nextInt(6) + 10;
        return x + weapon.getWeaponValue();

        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Unique " + weapon.getDescription();
        //TODO: Complete me
    }
}
