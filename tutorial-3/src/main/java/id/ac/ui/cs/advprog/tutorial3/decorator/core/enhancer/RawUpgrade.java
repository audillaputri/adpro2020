package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    Random rdm = new Random();

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        int x = rdm.nextInt(6) + 5;
        return x + weapon.getWeaponValue();
        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Raw " + weapon.getDescription();
        //TODO: Complete me
    }
}
