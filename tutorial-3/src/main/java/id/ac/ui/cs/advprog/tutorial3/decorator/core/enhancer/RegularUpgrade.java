package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    Random rdm = new Random();

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        int x = rdm.nextInt(5) + 1;
        return x + weapon.getWeaponValue();

        //TODO: Complete me
    }

    @Override
    public String getDescription() {
        return "Regular " + weapon.getDescription();
        //TODO: Complete me
    }
}
