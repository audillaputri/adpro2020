package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals(member.getName(), "Aqua");
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Goddess");
        //TODO: Complete me
    }

    @Test
    public void testMethodAddChildMember() {
        Member anon = new PremiumMember("anon", "orang");
        int before = member.getChildMembers().size();
        member.addChildMember(anon);
        assertEquals(member.getChildMembers().size(), before+1);
        //TODO: Complete me
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member anon = new PremiumMember("anon", "orang");
        int before = member.getChildMembers().size();
        member.addChildMember(anon);
        member.removeChildMember(anon);
        assertEquals(member.getChildMembers().size(), before);
        //TODO: Complete me
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        Member anon = new PremiumMember("anon", "orang");
        Member anon2 = new PremiumMember("anon2", "orang");
        Member anon3 = new PremiumMember("anon3", "orang");
        member.addChildMember(anon);
        member.addChildMember(anon2);
        member.addChildMember(anon3);

        int before = member.getChildMembers().size();
        Member anon4 = new PremiumMember("anon4", "orang");
        member.addChildMember(anon4);
        assertEquals(member.getChildMembers().size(), before);
        //TODO: Complete me
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member anonMaster = new PremiumMember("anonMaster", "Master");
        Member anon = new PremiumMember("anon", "orang");
        Member anon2 = new PremiumMember("anon2", "orang");
        Member anon3 = new PremiumMember("anon3", "orang");
        anonMaster.addChildMember(anon);
        anonMaster.addChildMember(anon2);
        anonMaster.addChildMember(anon3);

        int before = anonMaster.getChildMembers().size();
        Member anon4 = new PremiumMember("anon4", "orang");
        anonMaster.addChildMember(anon4);
        assertEquals(anonMaster.getChildMembers().size(), before +1);

        //TODO: Complete me
    }
}
