package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
<<<<<<< HEAD
        member = new OrdinaryMember("naura", "orang");
=======
        member = new OrdinaryMember("Nina", "Merchant");
>>>>>>> 9c6cd97e7f777eaefdccfa26a7b4ce9f10c53280
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "naura");
        //TODO: Complete me
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "orang");
        //TODO: Complete me
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        Member anon = new OrdinaryMember("anon", "orang");
        int before = member.getChildMembers().size();
        anon.addChildMember(anon);
        int after = member.getChildMembers().size();
        assertEquals(before, after);
        anon.removeChildMember(anon);
        int after2 = member.getChildMembers().size();
        assertEquals(after, before);
        //TODO: Complete me
    }
}
