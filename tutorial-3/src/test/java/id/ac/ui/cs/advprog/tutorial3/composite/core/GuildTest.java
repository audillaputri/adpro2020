package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        Member anon = new OrdinaryMember("anon", "orang");
        int before = guildMaster.getChildMembers().size();
        guild.addMember(guildMaster,anon);
        assertEquals(guildMaster.getChildMembers().size(), before+1);
    }

    @Test
    public void testMethodRemoveMember() {
        Member anon = new OrdinaryMember("anon", "orang");
        guild.addMember(guildMaster,anon);
        int before = guildMaster.getChildMembers().size();
        guild.removeMember(guildMaster,anon);
        assertEquals(guildMaster.getChildMembers().size(), before-1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member anon = new OrdinaryMember("anon", "orang");
        guild.addMember(guildMaster, anon);
        assertEquals(guild.getMember("anon", "orang"), anon);
        //TODO: Complete me
    }
}
