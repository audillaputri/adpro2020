package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    HolyGrail holyGrail;

    public void setUp() throws Exception {
        holyGrail = new HolyGrail();
        holyGrail.makeAWish("Adpro dapet A plis");
    }

    public void testSetWish(){
        holyGrail.makeAWish("Adpro dapet A plis");
        assertEquals("Adpro dapet A plis", holyGrail.getHolyWish().getWish());
    }

    public void testGetWish() {
        assertEquals("Adpro dapet A plis", holyGrail.getHolyWish().getWish());
    }


    // TODO create tests
}
