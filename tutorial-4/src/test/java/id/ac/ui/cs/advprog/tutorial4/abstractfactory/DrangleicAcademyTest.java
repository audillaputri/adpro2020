package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import  id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertNotNull(majesticKnight);
        assertNotNull(metalClusterKnight);
        assertNotNull(syntheticKnight);
        // TODO create test
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
        // TODO create test
    }

    @Test
    public void checkKnightDescriptions() {
        assertNull(majesticKnight.getSkill());
        assertNotNull(majesticKnight.getArmor());
        assertNotNull(majesticKnight.getWeapon());

        assertNull(metalClusterKnight.getWeapon());
        assertNotNull(metalClusterKnight.getArmor());
        assertNotNull(metalClusterKnight.getSkill());

        assertNull(syntheticKnight.getArmor());
        assertNotNull(syntheticKnight.getSkill());
        assertNotNull(syntheticKnight.getWeapon());

    }

}
