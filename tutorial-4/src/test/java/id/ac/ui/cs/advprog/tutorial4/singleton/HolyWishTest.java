package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
       HolyWish coba = HolyWish.getInstance();
       assertEquals(holyWish, coba);
    }

    @Test
    public void testWish(){
        holyWish.setWish("Pengen cepet lulus");
        assertEquals("Pengen cepet lulus", holyWish.getWish());

    }

    @Test
    public void testWishExist(){
        holyWish.setWish("ada");
        assertEquals("ada", holyWish.getWish());
        assertEquals("ada", holyWish.toString());
        assertEquals(holyWish.getWish(), holyWish.toString());
        assertNotNull(holyWish.getWish());

    }

}
