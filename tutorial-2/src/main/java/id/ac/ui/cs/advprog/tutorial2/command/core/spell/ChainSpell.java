package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

import org.springframework.boot.autoconfigure.web.ResourceProperties;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    public void cast(){
        for(Spell sp : spells) {
            sp.cast();
        }
    }

    public void undo(){
        for(int i = spells.size()-1;i>=0;i--){
            spells.get(i).undo();
        }

    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
